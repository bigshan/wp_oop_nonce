<?php

namespace Nonces\Test;

use Nonces\NonceGenerator;
use PHPUnit\Framework\TestCase;

/**
 * Tests for class NonceGenerator.
 */
class NonceGeneratorTest extends TestCase
{
    
    const DUMMY_ACTION= '_test_action';
    const DUMMY_NAME = '_test_name';        
    private static $dummyNonce;
    private static $dummyNG1;
    private static $dummyNG2;

    /**
    * Setting up the test environment.
    */
    protected function setUp(): void {
        self::$dummyNG1 = new NonceGenerator( self::DUMMY_ACTION );
        self::$dummyNG2 = new NonceGenerator( self::DUMMY_ACTION, self::DUMMY_NAME );
        self::$dummyNonce = \Nonces\wp_create_nonce( self::DUMMY_ACTION );
    }

    /**
    * Test the object instance.
    */
    public function testInstance(): void {
        $this->assertInstanceOf( NonceGenerator::class, self::$dummyNG2 );
        $this->assertInstanceOf( NonceGenerator::class, self::$dummyNG1 );
    }

    /**
    * Test the getter and setter for the action property.
    */
    public function testAction(): void {
        $ng = self::$dummyNG2;
        $this->assertSame( self::DUMMY_ACTION, $ng->getAction() );
    }

    /**
    * Test the getter and setter for the name property.
    */
    public function testName(): void {
        $ng = self::$dummyNG2;
        $this->assertSame( self::DUMMY_NAME, $ng->getName() );
    }

    
    /**
    * Test the generateNonce method used for the straight generation of the nonce.
    */
    public function testGenerateNonce(): void {
        $ng = self::$dummyNG1;
        $this->assertNull( $ng->getNonce() );
        $nonceGenerated = $ng->generateNonce();
        $this->assertSame( $nonceGenerated, self::$dummyNonce );
    }

    /**
    * Test the getter and setter for the nonce property.
    */
    public function testNonce(): void {
        $nonceGenerated = self::$dummyNG1->generateNonce();
        self::$dummyNG1->setNonce( '_test_new_nonce' );
        $this->assertNotEquals( $nonceGenerated, self::$dummyNG1->getNonce() );
        $this->assertSame( '_test_new_nonce', self::$dummyNG1->getNonce() );
    }
}