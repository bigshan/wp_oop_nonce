<?php 

namespace Nonces\Test;

use Nonces\NonceValidator;
use PHPUnit\Framework\TestCase;

/**
 * Tests for class NonceValidator.
 */
class NonceValidatorTest extends TestCase
{
	const DUMMY_ACTION = '_test_action';			
 	const DUMMY_NAME = '_test_name';
	private static $dummyNonce;
	private static $validator1;
	private static $validator2;

	/**
 	* Setting up the test environment.
 	*/
	protected function setUp(): void { 
 		self::$dummyNonce = \Nonces\wp_create_nonce( self::DUMMY_ACTION );
 		self::$validator1 = new NonceValidator( self::$dummyNonce, self::DUMMY_ACTION );
 		self::$validator2 = new NonceValidator( self::$dummyNonce, self::DUMMY_ACTION, self::DUMMY_NAME );		
 	}
	
	/**
 	* Test the object instance.
 	*/
    public function instance(): void {
		$this->assertInstanceOf( NonceValidator::class, self::$validator2 );
		$this->assertInstanceOf( NonceValidator::class, self::$validator1 );
	}

	/**
 	* Test the getter and setter for the action property.
 	*/
	public function testAction(): void {
 		$nv = self::$validator2;
 		$this->assertSame( self::DUMMY_ACTION, $nv->getAction() );
 	}

	/**
 	* Test the getter and setter for the name property.
 	*/
 	public function testName(): void {
 		$nv = self::$validator2;
 		$this->assertSame( self::DUMMY_NAME, $nv->getName() ); 	}
 	/**
 	* Test the validate method used for the straight validation of the nonce.
 	*/
 	public function testValidateNonce(): void {
 		$isValid = self::$validator1->validate();
 		$this->assertTrue( $isValid );
 	}
}