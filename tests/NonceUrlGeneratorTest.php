<?php 

namespace Nonces\Test;

use Nonces\NonceUrlGenerator;
use PHPUnit\Framework\TestCase;

/**
 * Tests for class NonceUrlGenerator.
 */
class NonceUrlGeneratorTest extends TestCase
{
	/** @var string */
	const DUMMY_ACTION  = '_test_action';
	/** @var string */
	const DUMMY_NAME = '_test_name';
	/** @var string */
	const DUMMY_URL = 'http://test.com';
	private static $dummyNonce;
	private static  $dummyNUG1;
	private static  $dummyNUG2;

	/**
 	* Setting up the test environment.
 	*/
	protected function setUp(): void {
 		self::$dummyNUG1 = new NonceUrlGenerator( self::DUMMY_URL, self::DUMMY_ACTION );
 		self::$dummyNUG2 = new NonceUrlGenerator( self::DUMMY_URL, self::DUMMY_ACTION, self::DUMMY_NAME );
 		self::$dummyNonce = \Nonces\wp_create_nonce( self::DUMMY_ACTION );
 	}

	/**
 	* Test the object instance.
 	*/
    public function testInstance(): void {
		$this->assertInstanceOf( NonceUrlGenerator::class, self::$dummyNUG1 );
		$this->assertInstanceOf( NonceUrlGenerator::class, self::$dummyNUG2 );
	}

	/**
 	* Test the getter and setter for the action property.
 	*/
	public function testAction(): void {
 		$nug = self::$dummyNUG2;
 		$this->assertSame( self::DUMMY_ACTION, $nug->getAction() );
 	}

 	/**
 	* Test the getter and setter for the name property.
 	*/
 	public function testName(): void { 
 		$nug = self::$dummyNUG2;
 		$this->assertSame( self::DUMMY_NAME, $nug->getName() );
 	}

 	
 	/**
 	* Test the generateNonce method used for the straight generation of the nonce.
 	*/
 	public function testGenerateNonce(): void {

		$nug = self::$dummyNUG1;
		$this->assertNull( $nug->getNonce() );
        $nonceGenerated = $nug->generateNonce();
		$this->assertSame( $nonceGenerated, self::$dummyNonce );
 	}

    /**
 	* Test the getter and setter for the nonce property.
 	*/
 	public function testNonce(): void {
 		$nug = self::$dummyNUG1;
 		$this->assertNull( $nug->getNonce() );
        $nonceGenerated = $nug->generateNonce();
        $nug->setNonce( '_test_new_nonce' );
        $this->assertNotEquals( $nonceGenerated, $nug->getNonce() );
 		$this->assertSame( '_test_new_nonce', $nug->getNonce() );
 	}

    /**
 	* Test the generateNonce_url method to build an url with a nonce query parameter to send via GET.
 	*/
 	public function testGenerateNonceUrl(): void{
        $urlGenerated = self::$dummyNUG1->generateNonceUrl();
        $urlExpected = self::DUMMY_URL.'?_wpnonce='. self::$dummyNonce;
		$this->assertSame( $urlGenerated, $urlExpected);
 	}
}