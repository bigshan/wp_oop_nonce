<?php 

namespace Nonces\Test;

use Nonces\NonceFieldGenerator;
use PHPUnit\Framework\TestCase;

/**
 * Tests for class NonceFieldGenerator.
 */
class NonceFieldGeneratorTest extends TestCase
{
	const DUMMY_ACTION = '_test_action';
	const DUMMY_NAME = '_test_name';
	private $dummyNonce;
	private $dummyNGF1;
	private $dummyNGF2;
	private $dummyNGF3;
	private $dummyNGF4;

	/**
 	* Setting up the test environment.
 	*/
	protected function setUp(): void {	
 		$this->dummyNGF1 = new NonceFieldGenerator( self::DUMMY_ACTION, '_wpnonce', false, false );
 		$this->dummyNGF2 = new NonceFieldGenerator( self::DUMMY_ACTION, self::DUMMY_NAME, false, false);
 		$this->dummyNGF3 = new NonceFieldGenerator( self::DUMMY_ACTION, self::DUMMY_NAME, true, false);
 		$this->dummyNGF4 = new NonceFieldGenerator( self::DUMMY_ACTION, self::DUMMY_NAME, false, true);
 		$this->dummyNonce = \Nonces\wp_create_nonce( self::DUMMY_ACTION );
 	}

	/**
 	* Test the object instance.
 	*/
    public function testInstance(): void {
		$this->assertInstanceOf( NonceFieldGenerator::class, $this->dummyNGF1 );
		$this->assertInstanceOf( NonceFieldGenerator::class, $this->dummyNGF2 );
		$this->assertInstanceOf( NonceFieldGenerator::class, $this->dummyNGF3 );
		$this->assertInstanceOf( NonceFieldGenerator::class, $this->dummyNGF4 );
	}

	/**
 	* Test the getter and setter for the action property.
 	*/
	public function testAction(): void {
 		$nfg = $this->dummyNGF2;
 		$this->assertSame( self::DUMMY_ACTION, $nfg->getAction() );
 	}

	/**
 	* Test the getter and setter for the name property.
 	*/
 	public function testName(): void {
 		$nfg = $this->dummyNGF2;
 		$this->assertSame( self::DUMMY_NAME, $nfg->getName() );
 	}

 	/**
 	* Test the generateNonce method used for the straight generation of the nonce.
 	*/
 	public function testGenerateNonce(): void {
		$nonceGenerated = $this->dummyNGF1->generateNonce();
		$this->assertSame( $nonceGenerated, $this->dummyNonce );
		$this->assertSame( $nonceGenerated, $this->dummyNGF1->getNonce() );
 	}

    /**
 	* Test the getter and setter for the nonce property.
 	*/
 	public function testNonce(): void {
 		$nfg = $this->dummyNGF1;
 		$this->assertNull( $nfg->getNonce() );
 		$nonceGenerated = $nfg->generateNonce();
 		$this->assertNotNull( $nfg->getNonce() );
 		$nfg->setNonce( '_test_new_nonce' );
 		$this->assertNotEquals( $nonceGenerated, $nfg->getNonce() );
 		$this->assertSame( '_test_new_nonce', $nfg->getNonce() );
 	}

    /**
 	* Test the generateNonceField method to build form field with a nonce parameter to send via POST.
 	*/
	public function testGenerateNonceField(): void {		
		$nfg = $this->dummyNGF1;
		$fieldGenerated = $nfg->generateNonceField();
		$fieldExpected = '<input type="hidden" id="_wpnonce" name="_wpnonce" value="' . $this->dummyNonce . '" />';
		$this->assertSame( $fieldGenerated, $fieldExpected );
 	}

    /**
 	* Test the generateNonceField method to build form field with a nonce parameter to send via POST. 
 	*/
 	public function testGenerateNonceFieldReferer(): void {
		$nfg = $this->dummyNGF3;
		$fieldGenerated = $nfg->generateNonceField();
		$fieldExpected = '<input type="hidden" id="'. self::DUMMY_NAME .'" name="'. self::DUMMY_NAME .'" value="' . $this->dummyNonce . '" /><input type="hidden" name="_wp_http_referer" value="my-url" />';
        $this->assertSame( $fieldGenerated, $fieldExpected);
 	}

    /**
 	* Test the generateNonceField method to build form field with a nonce parameter to send via POST.
 	*/
 	public function testGenerateNonceFieldEcho(): void {
 		$nfg = $this->dummyNGF4;
		$fieldExpected = '<input type="hidden" id="'. self::DUMMY_NAME .'" name="'. self::DUMMY_NAME .'" value="' . $this->dummyNonce . '" />';
 		$this->expectOutputString($fieldExpected);
		$fieldGenerated = $nfg->generateNonceField( false );
		$this->assertSame( $fieldGenerated, $fieldExpected);
 	}
}