<?php
/**
 * Nonce field generator file.
 * @package segun_adeniji/wp_nonce
 */

namespace Nonces;

/**
 * The class for the url generation with nonce query parameter (GET).
 */
class NonceUrlGenerator extends NonceGenerator {
	/**
	 * URL property.
	 * @var string 
	 */
	private  $url;


	/**
	 * Class constructor.
	 * @param    string $action     The nonce action value.
	 * @param    string $name       Optional. The nonce request name. Default = '_wpnonce'.
	 */
	public function __construct( string $url, string $action, string $name = '_wpnonce') {
		parent::__construct( $action, $name );
		$this->setUrl( $url );
	}

    /** @return string Url */
	public function getUrl(): string {
		return $this->url;
	}

	/** @param string Url */
	public function setUrl( string $url ): void {
		$this->url = $url;
	}

	/**
	 * Generate the url with nonce value.
	 * @return string $url URL with nonce action added.
	 */
	public function generateNonceUrl(): string {
		$this->generateNonce();
		return esc_html( add_query_arg( $this->getName(), $this->getNonce(), str_replace( '&amp;', '&', $this->getUrl() ) ) );
	}
}
