<?php
/**
 * Basic nonces functionality.
 *
 * @package segun_adeniji/wp_nonce
 */

namespace Nonces;

/**
 * The interface for the nonces basic functionality.
 */
interface NonceInterface {

	/** @return string Action */
	public function getAction(): string;

	/** @param string Action */
	public function setAction( string $action ): void;

	/** @return string Name */
	public function getName() : string;

    /** @param string Name */
	public function setName( string $name ): void;

	/**  @return string|null Nonce */
	public function getNonce(): ?string;

	/** @param string|null Nonce */
	public function setNonce( ?string $nonce ): void;
}
