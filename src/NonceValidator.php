<?php
/**
 * Nonce field generator file.
 * @package segun_adeniji/wp_nonce
 */

 namespace Nonces;

/**
 * The class for the nonce validation.
 */
final class NonceValidator extends Nonce {

	/**
	 * Class constructor.
	 * @param    string $action     The nonce action value.
	 * @param    string $name       Optional. The nonce request name. Default = '_wpnonce'.
	 * @param    string $nonce      The nonce value.
	 */
	public function __construct(string $nonce, string $action = '-1', string $name = '_wpnonce' ) {
		parent::__construct( $action, $name, $nonce );
	}

	/** @return  boolean */
	public function validate(): bool {
		return wp_verify_nonce( $this->getNonce(), $this->getAction() );
	}

}
