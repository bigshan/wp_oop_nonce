<?php
/**
 * Nonce field generator file.
 * @package segun_adeniji/wp_nonce
 */

 namespace Nonces;

/**
 * The class for the form nonce field generation (POST).
 */
final class NonceFieldGenerator extends NonceGenerator{
	/**
	 * Referer property.
	 * @var bool 
	 */
	private $referer;

	/**
	 * Echo property.
	 * @var bool 
	 */
	private $echo;

	/**
	 * Class constructor.
	 * @param    string $action       The nonce action value.
	 * @param    string $name         Optional. The nonce request name. Default = '_wpnonce'.
	 * @param    string $referer      Optional. Whether to set the referer field for validation. Default true.
	 * @param    string $echo         Optional. Whether to display or return hidden form field. Default true.
	 */
	public function __construct( string $action, string $name = '_wpnonce',  bool $referer = true, bool $echo = true) {
		parent::__construct( $action, $name );
		$this->setReferer( $referer );
		$this->setEcho( $echo );
	}

    /** @return bool Referer */
	public function getReferer(): bool {
		return $this->referer;
	}

	/** @param bool Referer */
	public function setReferer( bool $referer ): void {
		$this->referer = $referer;
	}

	/** @return bool Echo */
	public function getEcho(): bool {
		return $this->echo;
	}

	/** @param bool Echo */
	public function setEcho( bool $echo ): void {
		$this->echo = $echo;
	}

	/**
	 * Generate the form field/s with nonce value.
	 * @return string $fields             The nonce hidden form field
	 */
	public function generateNonceField(): string {
		$this->generateNonce();
		$nonceField = '<input type="hidden" id="' . $this->getName() . '" name="' . $this->getName() . '" value="' . esc_attr( $this->getNonce() ) . '" />';

		if ( $this->getReferer() ) {
			$nonceField .= wp_referer_field( false );
		}

		if ( $this->getEcho() ) {
			echo $nonceField;
		}

		return $nonceField;
	}
}
