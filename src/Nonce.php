<?php
/**
 * Basic nonce file.
 * @package segun_adeniji/wp_nonce
 */

 namespace Nonces;

/**
 * The abstract class for the nonces basic functionality.
 */
class Nonce implements NonceInterface {
    /**
	 * Action property.
	 * @var string 
	 */
	private $action;

	/**
	 * Name property.
	 * @var string 
	 */
	private $name;

	/**
	 * Nonce property.
	 * @var string|null 
	 */
	private $nonce;

	/**
	 * Class constructor.
	 * @param    string $action     The nonce action value.
	 * @param    string $name       Optional. The nonce request name. Default = '_wpnonce'.
	 * @param    string $nonce      Optional. The nonce request nonce. Default =  null.
	 */
	public function __construct( string $action, string $name = '_wpnonce', ?string $nonce = null) {
		$this->setAction( $action );
		$this->setName( $name );
		$this->setNonce( $nonce );
	}

	/** @return string Action */
	public function getAction(): string {
		return $this->action;
	}

	/** @param string Action */
	public function setAction( string $action ): void {
		$this->action = $action;
	}

	/** @return string Name */
	public function getName(): string {
		return $this->name;
	}

	/** @param string Name */
	public function setName( string $name ): void {
		$this->name = $name;
	}

	/**  @return string|null Nonce */
	public function getNonce(): ?string {
		return $this->nonce;
	}

	/** @param string|null Nonce */
	public function setNonce( ?string $nonce ): void {
		$this->nonce = $nonce;
	}
}