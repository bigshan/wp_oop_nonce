<?php
/**
 * Nonce field generator file.
 * @package segun_adeniji/wp_nonce
 */

namespace Nonces;

/**
 * The class for nonce generation.
 */
class NonceGenerator extends Nonce {

	/**
	 * Class constructor.
	 * @param    string $action     The nonce action value.
	 * @param    string $name       Optional. The nonce request name. Default = '_wpnonce'.
	 */
	public function __construct( string $action, string $name = '_wpnonce' ) {
		parent::__construct( $action, $name );
	}

	/**
	 * Nonce generation.
	 * @return int The generated nonce value.
	 */
	public function generateNonce(): ?string {
		$this->setNonce( wp_create_nonce( $this->getAction() ) );
		return $this->getNonce();
	}

}